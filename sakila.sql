-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 24. Feb 2016 um 14:11
-- Server Version: 5.5.46-0ubuntu0.14.04.2
-- PHP-Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `sakila`
--
CREATE DATABASE IF NOT EXISTS `sakila` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sakila`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `us`
--

DROP TABLE IF EXISTS `us`;
CREATE TABLE IF NOT EXISTS `us` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firma` varchar(100) DEFAULT '',
  `vorname` varchar(100) DEFAULT '',
  `nachname` varchar(100) DEFAULT '',
  `adresse` varchar(100) DEFAULT '',
  `ort` varchar(100) DEFAULT '',
  `stadt` varchar(100) DEFAULT '',
  `plz` varchar(100) DEFAULT '',
  `land` varchar(100) DEFAULT '',
  `phone1` varchar(100) DEFAULT '',
  `phone2` varchar(100) DEFAULT '',
  
  `email` varchar(100) DEFAULT '',
  `website` varchar(100) DEFAULT '',
  last_update TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


--
-- Tabellenstruktur für Tabelle `fl_insurance`
--

DROP TABLE IF EXISTS `fl_insurance`;
CREATE TABLE IF NOT EXISTS `fl_insurance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `statecode` varchar(45) DEFAULT '',
  `country` varchar(45) DEFAULT '',
  `latitude` float(10,2) DEFAULT NULL,
  `longitude` float(10,2) DEFAULT NULL,
  `line` varchar(45) DEFAULT '',
  `construction` varchar(45) DEFAULT '',
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


--
-- Tabellenstruktur für Tabelle `comuni`
--

DROP TABLE IF EXISTS `comuni`;
CREATE TABLE IF NOT EXISTS `comuni` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT '',
  `province` varchar(45) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
